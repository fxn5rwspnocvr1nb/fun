import sys

from fun.parse import parse
from fun.types import Package
from fun import std

if __name__ == '__main__':
    if len(sys.argv) > 1:
        with open(sys.argv[1], 'r') as f:
            std.import_(f.read())
        exit()
    print('Fun 11.45.14 on win32')
    while True:
        ret = std.import_(input('> '), *sys.argv)
        std.set('_', ret)
        if ret is not None:
            print(ret)