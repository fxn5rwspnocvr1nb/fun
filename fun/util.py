def isfloat(s: str) -> bool:
    if not s[0].isnumeric():
        return False
    for c in s:
        if not c.isnumeric() or c == '.':
            return False
    return True